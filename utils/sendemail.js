const nodemailer = require('nodemailer');
const { email } = require('../config/default')

// 使用async..await 创建执行函数
module.exports = {
  sendEmail: async ({
    toEmail,
    emailCode
  }) => {
    // 如果你没有一个真实邮箱的话可以使用该方法创建一个测试邮箱
    // let transporter = await nodemailer.createTestAccount();

    // 创建Nodemailer传输器 SMTP 或者 其他 运输机制
    let transporter = nodemailer.createTransport(email);

    // 定义transport对象并发送邮件
    const res = await transporter.sendMail({
      from: '"ghost" <15083328977@163.com>', // 发送方邮箱的账号
      to: toEmail, // 邮箱接受者的账号
      subject: "2048验证码", // Subject line
      text: `欢迎登录2048，您的验证码是：${emailCode}`, // 文本内容
      html: `欢迎登录2048, 您的邮箱验证码是:<b>${emailCode}</b>`, // html 内容, 如果设置了html内容, 将忽略text内容
    });

    return res.response.includes("OK") ? true : res
  }

}
// main().catch(console.error);
