const jwt = require('jsonwebtoken')
const { loginConf } = require('../config/default')
const dayjs = require('dayjs')

async function check(ctx, next) {

  let url = ctx.url.split('?')[0]
  if(!url.startsWith('/api')){
   return await next()
  }
  if(url.includes('score')){
    // 否则获取到token
    // let token = ctx.request.headers["authorization"]
    let token = ctx.cookies.get('token')

    if (token) {
      // 如果有token的话就开始解析
      const tokenItem = jwt.verify(token, loginConf.tokenSecret)
      // 将token的创建的时间和过期时间结构出来
      const { time, timeout } = tokenItem
      console.log('token过期时间', dayjs().add(timeout, 'ms').format('YYYYMMDD hh:mm:ss'))
      // 拿到当前的时间
      let data = new Date().getTime();
      // 判断一下如果当前时间减去token创建时间小于或者等于token过期时间，说明还没有过期，否则过期
      if (data - time <= timeout) {
        // token没有过期
        await next()
      } else {
        ctx.body = {
          code: 202,
          msg: 'token 已过期，请重新登陆'
        }
        // await next()
      }
    } else {
      ctx.body = {
        code: 201,
        msg: '未登录'
      }
      // await next()
    }
    if (url == '/api/score') {
      await next()
    }
  } else {
    await next()
  }
}

module.exports = check
